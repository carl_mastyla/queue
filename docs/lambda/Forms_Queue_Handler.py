from __future__ import print_function

import json
import boto3
import urllib
import urllib2
import httplib
import base64

print('Loading function')

s3Client = boto3.client('s3')

def lambda_handler(event, context):
    try:
        # Get the object from the event and show its content type
        bucketName = event['Records'][0]['s3']['bucket']['name']
        key = urllib.unquote_plus(event['Records'][0]['s3']['object']['key'].encode('utf8'))
        indexObject = key.rfind('/')
        filePath = key[:indexObject]
        print('FilePath: ' + filePath)
        fileName = key[indexObject+1:]
        print('FileName: ' + fileName)
        fileAndPath = filePath + '/' + fileName
        print('File and path: ' + fileAndPath)
        archiveFileAndPath = filePath + '/archive/' + fileName
        print('Archive File and path: ' + archiveFileAndPath)
        failureFileAndPath = filePath + '/failure/' + fileName
        print('Failure File and path: ' + failureFileAndPath)
        # Retrive s3 file content
        s3Resp = s3Client.get_object(Bucket=bucketName, Key=key)
        print('Object: ' + key)
        s3FileContent = s3Resp['Body'].read()
        print("S3 file content: " + s3FileContent)
    except Exception as e:
        print('EXCEPTION: Exception getting object from bucket, details: ', e)
        print('ERROR DETAILS: Error getting object {} from bucket {}. Make sure they exist and your bucket is in the same region as this function.'.format(key, bucketName))
        #raise e
    else:
        try:
            s3FileJson = json.loads(s3FileContent)
            formId = s3FileJson['formId']
        except Exception as e:
            print('EXCEPTION: Exception parsing json content from bucket, details: ', e)
            move_file(bucketName, fileAndPath, failureFileAndPath)
            #raise e
        else:
            try:
                # Retrieve routing form details
                formsUrl = "https://api.cm.astrazeneca.com/forms_dev/v1/forms/" + formId
                formsRequest = urllib2.Request(formsUrl)
                base64string = base64.encodestring('%s:%s' % ('forms_service', 'VUbK0kokmDaDjMi')).replace('\n', '')
                formsRequest.add_header("Authorization", "Basic %s" % base64string) 
                formsResp = urllib2.urlopen(formsRequest)
            except urllib2.HTTPError, e:
                print('EXCEPTION: Exception HTTPError retrieving the form "{}", details: '.format(formId), e)
                move_file(bucketName, fileAndPath, failureFileAndPath)
                errorResp = e.read()
                erroJson = json.loads(errorResp)
                print('ERROR DETAILS: ' + erroJson['description'])
                #raise e
            except urllib2.URLError, e:
                print('EXCEPTION: Exception URLError retrieving the form "{}", details: '.format(formId), e)
                move_file(bucketName, fileAndPath, failureFileAndPath)
                #raise e
            except httplib.HTTPException, e:
                print('EXCEPTION: Exception HTTPException retrieving the form "{}", details: '.format(formId), e)
                move_file(bucketName, fileAndPath, failureFileAndPath)
                #raise e
            except Exception as e:
                print('EXCEPTION: Exception in the form section, details: ', e)
                move_file(bucketName, fileAndPath, failureFileAndPath)
                #raise e
            else:
                try:
                    formsData = formsResp.read()
                    print("Form retrieved: " + formsData)
                    formsJson = json.loads(formsData)
                    routingEndpoint = formsJson['routing']['routingEndpoint']
                    print("routingEndpoint: " + routingEndpoint)
                    bearerToken = formsJson['routing']['bearerToken']
        
                    # Post s3 file content to the target pipeline
                    headers = {'Authorization': bearerToken}
                    serviceReq = urllib2.Request(routingEndpoint, s3FileContent, headers)
                    serviceResp = urllib2.urlopen(serviceReq)
                    print("POSTING DONE")
                except urllib2.HTTPError, e:
                    print('EXCEPTION: Exception HTTPError posting to the routing endpoint "{}", details: '.format(routingEndpoint), e)
                    move_file(bucketName, fileAndPath, failureFileAndPath)
                    errorResp = e.read()
                    erroJson = json.loads(errorResp)
                    for item in erroJson['snap_map']:
                        if erroJson['snap_map'][item]['state'] == 'Failed':
                            print('ERROR DETAILS: ' + erroJson['snap_map'][item]['reason'])
                    #raise e
                except urllib2.URLError, e:
                    print('EXCEPTION: Exception URLError posting to the routing endpoint "{}", details: '.format(routingEndpoint), e)
                    move_file(bucketName, fileAndPath, failureFileAndPath)
                    #raise e
                except httplib.HTTPException, e:
                    print('EXCEPTION: Exception HTTPException posting to the routing endpoint "{}", details: '.format(routingEndpoint), e)
                    move_file(bucketName, fileAndPath, failureFileAndPath)
                    #raise e
                except Exception as e:
                    print('EXCEPTION: Exception in the routing endpoint section, details: ', e)
                    move_file(bucketName, fileAndPath, failureFileAndPath)
                    #raise e
                else:
                    move_file(bucketName, fileAndPath, archiveFileAndPath)
                    serviceData = serviceResp.read()
                    print("SUCCESS: " + serviceData)
                    #return s3Resp['ContentType']
        
def move_file(bucketName, fileAndPath, tarjetFileAndPath):
    try:
        s3 = boto3.resource('s3')
        copy_source = {'Bucket': bucketName,'Key': fileAndPath}
        s3.meta.client.copy(copy_source, bucketName, tarjetFileAndPath)
        s3.meta.client.delete_object(Bucket=bucketName, Key=fileAndPath)
        print("FILE MOVED from " + fileAndPath + " to " + tarjetFileAndPath)
    except Exception as e:
        print('EXCEPTION: Exception moving the file to {}, details: '.format(tarjetFileAndPath), e)
        #raise e